package pl.akozlowska.service;

import pl.akozlowska.dao.RoomDao;
import pl.akozlowska.model.Room;

import java.util.List;
import java.util.stream.Collectors;

public class RoomService {
    private RoomDao roomDao;

    public RoomService (RoomDao roomDao){
        this.roomDao = roomDao;
    }

    public List<Room> getAll(){
        return roomDao.getAll();
    }

    public   List<Room> getOnePersonRoomsByPrice (){
      return roomDao.getOnePersonRoomsByPrice();
    }

    public   List<Room> getTwoPersonsRoomsByPrice (){
        return roomDao.getTwoPersonsRoomsByPrice();
    }

    public   List<Room> getThreePersonsRoomsByPrice (){
        return roomDao.getThreePersonsRoomsByPrice();
    }

    public   List<Room> getFourPersonsRoomsByPrice (){
        return roomDao.getFourPersonsRoomsByPrice();
    }




}
