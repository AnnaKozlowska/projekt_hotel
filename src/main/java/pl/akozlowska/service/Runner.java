package pl.akozlowska.service;

import pl.akozlowska.gui.Gui;
import pl.akozlowska.gui.impl.ConsoleGui;

public class Runner {

    public static void main(String[] args) {
        Gui gui = new ConsoleGui();
        gui.showMainMenu();
    }
}
