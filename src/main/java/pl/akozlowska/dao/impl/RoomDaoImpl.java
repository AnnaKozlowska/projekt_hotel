package pl.akozlowska.dao.impl;

import pl.akozlowska.dao.RoomDao;
import pl.akozlowska.model.Room;
import pl.akozlowska.model.RoomHistoryRent;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class RoomDaoImpl implements RoomDao {
    private List<Room> rooms;

    public RoomDaoImpl (){
        this.rooms = new ArrayList<>();
        this.rooms.add(new Room(1, 2, 120, 20, true));
        this.rooms.add(new Room(2, 2, 120, 20, true));
        this.rooms.add(new Room(3, 3, 150, 25, true));
        this.rooms.add(new Room(4, 1, 80, 18, true));
        this.rooms.add(new Room(5, 1, 60, 15, false));
        this.rooms.add(new Room(6, 1, 60, 15, false));
        this.rooms.add(new Room(7, 2, 140, 25, true));
        this.rooms.add(new Room(8, 3, 170, 30, true));
        this.rooms.add(new Room(9, 2, 120, 20, true));
        this.rooms.add(new Room(10, 2, 140, 25, true));
        this.rooms.add(new Room(11, 2, 120, 20, true));
        this.rooms.add(new Room(12, 4, 250, 40, true));
        this.rooms.add(new Room(13, 4, 250, 40, true));
        this.rooms.add(new Room(14, 3, 200, 30, true));
        this.rooms.add(new Room(15, 2, 120, 20, true));
        this.rooms.add(new Room(16, 2, 120, 20, true));
    }


    @Override
    public List<Room> getAll() {
        return new ArrayList<>(rooms);
    }

    @Override
    public List<Room> getOnePersonRoomsByPrice() {
        return getAll().stream()
                .filter(e -> e.getMaxGuestAmount() == 1)
                .sorted((x,y) -> x.getPricePerNight() - y.getPricePerNight())
                .collect(Collectors.toList());
    }

    @Override
    public List<Room> getTwoPersonsRoomsByPrice() {
        return getAll().stream()
                .filter(e -> e.getMaxGuestAmount() == 2)
                .sorted((x,y) -> x.getPricePerNight() - y.getPricePerNight())
                .collect(Collectors.toList());
    }

    @Override
    public List<Room> getThreePersonsRoomsByPrice() {
        return getAll().stream()
                .filter(e -> e.getMaxGuestAmount() == 3)
                .sorted((x,y) -> x.getPricePerNight() - y.getPricePerNight())
                .collect(Collectors.toList());
    }

    @Override
    public List<Room> getFourPersonsRoomsByPrice() {
        return getAll().stream()
                .filter(e -> e.getMaxGuestAmount() == 4)
                .sorted((x,y) -> x.getPricePerNight() - y.getPricePerNight())
                .collect(Collectors.toList());
    }


    @Override
    public void rentRoom(Integer amountOfPersons, RoomHistoryRent historyRent) {
        Optional<Room> any = getAll().stream()
                .filter(e -> e.getMaxGuestAmount() >= amountOfPersons)
                .findAny();

//        if(any.isPresent()){
//            //mmmm
//        } else
//
//            //
//
//        any
//        return any


        // ...............

    }
}
