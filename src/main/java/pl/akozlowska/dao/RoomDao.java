package pl.akozlowska.dao;

import pl.akozlowska.model.Room;
import pl.akozlowska.model.RoomHistoryRent;

import java.util.List;

public interface RoomDao {

    List<Room> getAll();
    List<Room> getOnePersonRoomsByPrice ();
    List<Room> getTwoPersonsRoomsByPrice ();
    List<Room> getThreePersonsRoomsByPrice ();
    List<Room> getFourPersonsRoomsByPrice ();

    void rentRoom(Integer amountOfPersons, RoomHistoryRent historyRent);

}
