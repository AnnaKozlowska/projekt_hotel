package pl.akozlowska.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Room implements Serializable {

    private int id;
    private int maxGuestAmount;
    private int pricePerNight;
    private int areaM2;
    private boolean hasPrivateToilet;


    public Room(int id, int maxGuestAmount, int pricePerNight, int areaM2, boolean hasPrivateToilet) {
        this.id = id;
        this.maxGuestAmount = maxGuestAmount;
        this.pricePerNight = pricePerNight;
        this.areaM2 = areaM2;
        this.hasPrivateToilet = hasPrivateToilet;
    }

    private List<RoomHistoryRent> roomHistoryRents = new ArrayList<>();


    private int GetPrice (int day){
        return day * pricePerNight;
    }







}
