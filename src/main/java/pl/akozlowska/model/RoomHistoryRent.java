package pl.akozlowska.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class RoomHistoryRent {

    private LocalDate startDay;
    private LocalDate endDay;




}
