package pl.akozlowska.gui.impl;

import pl.akozlowska.dao.impl.RoomDaoImpl;
import pl.akozlowska.gui.Gui;
import pl.akozlowska.service.RoomService;

import java.util.Scanner;

public class ConsoleGui implements Gui {
    private static Scanner scanner = new Scanner(System.in);

    private RoomService roomService = new RoomService((new RoomDaoImpl()));


    @Override
    public void showMainMenu() {
        Integer choosed = null;

        do {
            printMainOptions();

            choosed = getInt();
            switch (choosed) {

                case 1:
                    offertMenu();
                    break;
                case 2:
                    ReservationConsoleGui reservationConsoleGui = new ReservationConsoleGui();
                    reservationConsoleGui.showMainMenu();
                    break;
                case 3:
                    getDescription ();
                    break;
                case 4:
                    System.out.println("Zapraszamy ponownie");
                default:
                    System.out.println("Wybierz ponownie");


            }
        } while (choosed != 4);


    }

    private void printMainOptions() {
        System.out.println("Wytamy w hotelu Java! Wybierz opcję, która Cię interesuje.");
        System.out.println("1 - Przejrzyj ofertę");
        System.out.println("2 - Rezerwacja pokoi");
        System.out.println("3 - Udogodnienia");
        System.out.println("4 - Wyjście");
    }

    private Integer getInt() {
        String s = scanner.nextLine();
        return Integer.parseInt(s);
    }

    private void getOffert (){
        System.out.println("1 - Pokoje jednoosobowe");
        System.out.println("2 - Pokoje dwuosobowe");
        System.out.println("3 - Pokoje trzyosobowe");
        System.out.println("4 - Pokoje czteroobowe");
        System.out.println("5 - Wszystkie pokoje");
        System.out.println("6 - Wstecz");
    }

    private void offertMenu() {
        System.out.println("Nasza oferta: ");
        getOffert();
        int choosed = getInt();
        switch (choosed) {
            case 1:
                System.out.println("Pokoje jednoosobowe: ");
                System.out.println(roomService.getOnePersonRoomsByPrice());
                break;
            case 2:
                System.out.println("Pokoje dwuosobowe:  ");
                System.out.println(roomService.getTwoPersonsRoomsByPrice());
                break;
            case 3:
                System.out.println("Pokoje trzyosobowe: ");
                System.out.println(roomService.getThreePersonsRoomsByPrice());
                break;
            case 4:
                System.out.println("Pokoje czterosobowe: ");
                System.out.println(roomService.getFourPersonsRoomsByPrice());
                break;
            case 5:
                System.out.println("Cała oferta: ");
                System.out.println(roomService.getAll());
                break;
            case 6:
                return;
            default:
                System.out.println("Wybierz ponownie");
        }
        backMenu();
    }
    private void backMenu(){
        System.out.println("1 - wstecz");
        System.out.println("2 - wyjdz");
        Integer anInt = getInt();
        if (anInt == 2){
            System.exit(0);
        }
    }
    private void getAvailableOffert (){
        System.out.println("1 - Pokoje jednoosobowe");
        System.out.println("2 - Pokoje dwuosobowe");
        System.out.println("3 - Pokoje trzyosobowe");
        System.out.println("4 - Pokoje czteroobowe");
        System.out.println("5 - Wszystkie pokoje");
        System.out.println("6 - Wstecz");
    }

    private void availableMenu() {
        System.out.println("Pokoje dostępne do wynajęcia: ");
        getAvailableOffert();
        int choosed = getInt();
        switch (choosed){
            case 1:
                System.out.println("Pokoje jednoosobowe: ");
                break;
            case 2:
                System.out.println("Pokoje dwuosobowe:  ");
                break;
            case 3:
                System.out.println("Pokoje trzyosobowe: ");
                break;
            case 4:
                System.out.println("Pokoje czterosobowe: ");
                break;
            case 5:
                System.out.println("Wszystkie pokoje: ");
                break;
            case 6:
                printMainOptions();
                break;
            default:
                System.out.println("Wybierz ponownie");
        }
        backMenu();
    }

    private void getDescription(){
        System.out.println(" opiss bla blabla");
    }

}
